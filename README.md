# Open Mobility indicator Jupyter notebook "your indicator"

## description : [indicator.yaml file](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/edit/main/indicator.yaml)
(! change the above URL with https://gitlab.com/open-mobility-indicators/indicators/yourindicator/-/blob/main/indicator.yaml)

This text will describe which data your indicator notebook is creating.

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute) : see the wiki doc.](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)  
