#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi


echo "download data to target dir: $TARGET_DIR"


# Your code here (example below)
# you can replace the code below with echo "this script download.sh does nothing : downloads are done by the notebook later on"


ZIP_URL=`jq -r '.zip_url' $PARAMETER_PROFILE_FILE`
ZIP_FILE_NAME=`jq -r '.zip_filename' $PARAMETER_PROFILE_FILE`


wget "$ZIP_URL" -O $TARGET_DIR/$ZIP_FILE_NAME

echo "done."
